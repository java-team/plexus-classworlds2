Source: plexus-classworlds2
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Ludovic Claude <ludovic.claude@laposte.net>,
           Damien Raude-Morvan <drazzib@debian.org>
Build-Depends: ant-optional,
               debhelper (>= 9),
               default-jdk,
               maven-ant-helper (>> 6.7),
               maven-repo-helper (>= 1.5~)
Build-Depends-Indep: default-jdk-doc
Standards-Version: 3.9.6
Vcs-Git: git://anonscm.debian.org/pkg-java/plexus-classworlds2.git
Vcs-Browser: http://anonscm.debian.org/cgit/pkg-java/plexus-classworlds2.git
Homepage: http://plexus.codehaus.org/plexus-classworlds

Package: libplexus-classworlds2-java
Architecture: all
Depends: ${misc:Depends}
Suggests: libplexus-classworlds2-java-doc
Description: Class loading utilities for the Plexus framework
 The Plexus project provides a full software stack for creating and executing
 software projects. Based on the Plexus container, the applications can
 utilise component-oriented programming to build modular, reusable components
 that can easily be assembled and reused.
 .
 While Plexus is similar to other inversion-of-control (IoC) or dependency
 injection frameworks such as the Spring Framework, it is a full-fledged
 container that supports many more features such as:
 .
  * Component lifecycles
  * Component instantiation strategies
  * Nested containers
  * Component configuration
  * Auto-wiring
  * Component dependencies, and
  * Various dependency injection techniques including constructor injection,
   setter injection, and private field injection.

Package: libplexus-classworlds2-java-doc
Architecture: all
Section: doc
Depends: default-jdk-doc, ${misc:Depends}
Suggests: libplexus-classworlds2-java
Description: Class loading utilities for the Plexus framework - documentation
 The Plexus project provides a full software stack for creating and executing
 software projects. Based on the Plexus container, the applications can
 utilise component-oriented programming to build modular, reusable components
 that can easily be assembled and reused.
 .
 While Plexus is similar to other inversion-of-control (IoC) or dependency
 injection frameworks such as the Spring Framework, it is a full-fledged
 container that supports many more features such as:
 .
  * Component lifecycles
  * Component instantiation strategies
  * Nested containers
  * Component configuration
  * Auto-wiring
  * Component dependencies, and
  * Various dependency injection techniques including constructor injection,
   setter injection, and private field injection.
 .
 This package provides the API documentation for libplexus-classworlds2-java.
